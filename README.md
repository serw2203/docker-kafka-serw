Kafka in Docker

CLEAR
```bash
sudo docker system prune -a
```

BUILD
```bash
sudo docker build kafka/ -t serw/kafka
```

RUN
```bash
sudo docker run -d --rm -p 2181:2181 -p 9092:9092 --name kafka serw/kafka
//sudo docker run -it --rm -p 2181:2181 -p 9092:9092 --name kafka serw/kafka
```

LOG
```bash
sudo docker logs -f kafka
```

CREATE TOPIC (run after first)
```bash
sudo docker exec kafka /opt/kafka_2.11-0.11.0.2/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test
```

PRODUCER
```bash
sudo docker run -it --rm  --link kafka serw/kafka /opt/kafka_2.11-0.11.0.2/bin/kafka-console-producer.sh --broker-list kafka:9092 --topic test
```

CONSUMER
```bash
sudo docker run -it --rm --link kafka serw/kafka /opt/kafka_2.11-0.11.0.2/bin/kafka-console-consumer.sh --bootstrap-server kafka:9092 --topic test --from-beginning
```
